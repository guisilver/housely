<?php

use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * AccessMiddleware
 *
 * Access and user permissions
 */
class AccessMiddleware extends DefaultController implements MiddlewareInterface
{
    public function call(Phalcon\Mvc\Micro $app)
    {
        // Initialize
        // Gets users ACL
        include APP_PATH . '/app/config/acl.php';
        $arrHandler = $app->getActiveHandler();
        //get the controller for this handler
        $array = (array) $arrHandler[0];
        $nameController = implode('', $array);
        $controller = str_replace('Controller', '', $nameController);
        // check if controller is Index, if it´s Index, then checks if any of functions are called if so return allow
        if ($controller === 'Index') {
            $allowed = 1;
            return $allowed;
        }

        // gets user token
        $myToken = $this->getToken();

        // Verifies Token exists and is not empty
        if (empty($myToken) || $myToken == '') {
            $this->buildErrorResponse('Error', 400, 'common.EMPTY_TOKEN_OR_NOT_RECEIVED');
        }
        // Verifies Token
        try {
            $tokenDecoded = $this->decodeToken($myToken);
            // Verifies User role Access
            $allowedAccess = $acl->isAllowed($tokenDecoded->role, $controller, $arrHandler[1]);
            return (!$allowedAccess) ? $this->buildErrorResponse('Error', 403, 'common.YOUR_USER_ROLE_DOES_NOT_HAVE_THIS_FEATURE') : $allowedAccess;
        } catch (\Throwable $e) {
            // BAD TOKEN
            $this->buildErrorResponse('Error', 401, 'common.BAD_TOKEN_GET_A_NEW_ONE');
        }
    }
}
