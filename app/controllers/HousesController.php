<?php

class HousesController extends DefaultController
{

    private $housesService;

    public function onConstruct()
    {
        $this->housesService = new HousesService($this->modelsManager);
    }

    function list() {
        try {
            $this->initializeGet();
            $options = $this->buildOptions('Houses.id asc', $this->request->get('sort'), $this->request->get('order'), $this->request->get('limit'), $this->request->get('offset'));

            $data = json_decode($this->request->getRawBody());

            $parameters["search"] = '%' . $data->search . '%';
            $parameters["minimalBedroomsCount"] = $data->minimalBedroomsCount;
            $parameters["minimalToiletCount"] = $data->minimalToiletCount;
            $conditions = null;

            $houses = $this->housesService->list(
                $conditions,
                $parameters,
                $options['orderBy'],
                $options['offset'],
                $options['limit'],
            );
            $this->buildSuccessResponseRawData($houses);
        } catch (\Exception $e) {
            $this->buildErrorResponse("Error", $e->getCode(), $e->getMessage());
        }
    }

    public function get($id)
    {
        try {
            $this->initializeGet();
            $house = $this->housesService->get($id);
            $this->buildSuccessResponseRawData($house);
        } catch (\Exception $e) {
            $this->buildErrorResponse("Error", $e->getCode(), $e->getMessage());
        }
    }

    public function create()
    {
        try {
            $this->initializePost();
            $data = json_decode($this->request->getRawBody());
            $user = $this->getAuthenticatedUser();
            $house = $this->housesService->create($data, $user);
            $this->buildSuccessResponseRawData($house);
        } catch (\Exception $e) {
            $this->buildErrorResponse("Error", $e->getCode(), $e->getMessage());
        }
    }

    public function update($id)
    {
        try {
            $this->initializePatch();
            $data = json_decode($this->request->getRawBody());
            $user = $this->getAuthenticatedUser();
            $house = $this->housesService->update($id, $data, $user);
            $this->buildSuccessResponseRawData($house);
        } catch (\Exception $e) {
            $this->buildErrorResponse("Error", $e->getCode(), $e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->initializeDelete();
            $user = $this->getAuthenticatedUser();
            $house = $this->housesService->get($id);
            $this->housesService->delete($id, $user);
            $this->buildSuccessResponse(200, 'common.DELETED_SUCCESSFULLY');
        } catch (\Exception $e) {
            $this->buildErrorResponse("Error", $e->getCode(), $e->getMessage());
        }
    }
}
