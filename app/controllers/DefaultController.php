<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\Query\Builder;

class DefaultController extends Controller
{
    /**
     * These functions are available for multiple controllers
     */

    /**
     * Try to save data in DB
     */
    public function tryToSaveData($element, $customMessage = 'common.THERE_HAS_BEEN_AN_ERROR')
    {
        if (!$element->save()) {
            // Send errors
            $errors = array();
            foreach ($element->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $this->buildErrorResponse('Error', 500, $customMessage, $errors);
        }
        return true;
    }

    /**
     * Try to delete data in DB
     */
    public function tryToDeleteData($element)
    {
        if (!$element->delete()) {
            // Send errors
            $errors = array();
            foreach ($element->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $this->buildErrorResponse('Error', 500, 'common.COULD_NOT_BE_DELETED', $errors);
        }
        return true;
    }

    /**
     * Build options for listings
     */
    public function buildOptions($defaultSort, $sort, $order, $limit, $offset)
    {
        $options = [];
        $rows = 5;
        $orderBy = $defaultSort;
        $offset = 0;
        $limit = $offset + $rows;

        // Handles Sort querystring (orderBy)
        if ($sort != null && $order != null) {
            $orderBy = $sort . ' ' . $order;
        }

        // Gets rows_per_page
        if ($this->request->get('limit') != null) {
            $rows = $this->getQueryLimit($this->request->get('limit'));
            $limit = $rows;
        }

        // Calculate the offset and limit
        if ($this->request->get('offset') != null) {
            $offset = $this->request->get('offset');
            $limit = $rows;
        }
        $options = $this->array_push_assoc($options, 'rows', $rows);
        $options = $this->array_push_assoc($options, 'orderBy', $orderBy);
        $options = $this->array_push_assoc($options, 'offset', $offset);
        $options = $this->array_push_assoc($options, 'limit', $limit);
        return $options;
    }

    /**
     * Build listing object
     */
    public function buildListingObject($elements, $rows, $total)
    {
        $data = [];
        $data = $this->array_push_assoc($data, 'rows_per_page', $rows);
        $data = $this->array_push_assoc($data, 'total_rows', $total);
        $data = $this->array_push_assoc($data, 'rows', (is_array($elements) ? $elements : $elements->toArray()));
        return $data;
    }

    /**
     * Generated NOW datetime based on a timezone
     */
    public function getNowDateTime()
    {
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone('UTC'));
        $now = $now->format('Y-m-d H:i:s');
        return $now;
    }

    /**
     * Generated NOW datetime based on a timezone and added XX minutes
     */
    public function getNowDateTimePlusMinutes($minutesToAdd)
    {
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone('UTC'));
        $now->add(new DateInterval('PT' . $minutesToAdd . 'M'));
        $now = $now->format('Y-m-d H:i:s');
        return $now;
    }

    /**
     * Array push associative.
     */
    public function array_push_assoc($array, $key, $value)
    {
        $array[$key] = $value;
        return $array;
    }

    /**
     * Generates limits for queries.
     */
    public function getQueryLimit($limit)
    {
        $setLimit = 5;
        if ($limit != '') {
            if ($limit > 150) {
                $setLimit = 150;
            }
            if ($limit <= 0) {
                $setLimit = 1;
            }
            if (($limit >= 1) && ($limit <= 150)) {
                $setLimit = $limit;
            }
        }
        return $setLimit;
    }

    /**
     * Verifies if is get request
     */
    public function initializeGet()
    {
        if (!$this->request->isGet()) {
            die();
        }
    }

    /**
     * Verifies if is post request
     */
    public function initializePost()
    {
        if (!$this->request->isPost()) {
            die();
        }
    }

    /**
     * Verifies if is patch request
     */
    public function initializePatch()
    {
        if (!$this->request->isPatch()) {
            die();
        }
    }

        /**
     * Verifies if is put request
     */
    public function initializePut()
    {
        if (!$this->request->isPut()) {
            die();
        }
    }

    /**
     * Verifies if is patch request
     */
    public function initializeDelete()
    {
        if (!$this->request->isDelete()) {
            die();
        }
    }

    /**
     * Encode token.
     */
    public function encodeToken($data)
    {
        // Encode token
        $tokenEncoded = $this->jwt->encode($data, $this->tokenConfig['secret']);
        $tokenEncoded = $this->mycrypt->encryptBase64($tokenEncoded);
        return $tokenEncoded;
    }

    /**
     * Decode token.
     */
    public function decodeToken($token)
    {
        // Decode token
        $token = $this->mycrypt->decryptBase64($token);
        $token = $this->jwt->decode($token, $this->tokenConfig['secret'], array('HS256'));
        return $token;
    }

    /**
     * Returns token from the request.
     * Uses token URL query field, or Authorization header
     */
    public function getToken()
    {
        $authHeader = $this->request->getHeader('Authorization');
        $authQuery = $this->request->getQuery('token');
        return $authQuery ? $authQuery : $this->parseBearerValue($authHeader);
    }

    public function buildSuccessResponseRawData($data)
    {
        $this->response->setJsonContent($data, JSON_NUMERIC_CHECK)->send();
        die();
    }

        /**
     * Builds success responses.
     */
    public function buildSuccessResponse($code, $messages, $data = '')
    {
        switch ($code) {
            case 200:
                $status = 'OK';
                break;
            case 201:
                $status = 'Created';
                break;
            case 202:
                break;
        }
        $generated = array(
            'status' => $status,
            'code' => $code,
            'messages' => $messages,
            'data' => $data,
        );
        $this->response->setStatusCode($code, $status)->sendHeaders();
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent($generated, JSON_NUMERIC_CHECK)->send();
        die();
    }

    /**
     * Builds error responses.
     */
    public function buildErrorResponse($status, $code, $messages, $data = '')
    {
        switch ($code) {
            case 400:
                $status = 'Bad Request';
                break;
            case 401:
                $status = 'Unauthorized';
                break;
            case 403:
                $status = 'Forbidden';
                break;
            case 404:
                $status = 'Not Found';
                break;
            case 409:
                $status = 'Conflict';
                break;
            case 500:
                $status = 'Internal Server Error';
                break;
        }
        $generated = array(
            'status' => $status,
            'code' => $code,
            'messages' => $messages,
            'data' => $data,
        );
        $this->response->setStatusCode($code, $status)->sendHeaders();
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent($generated, JSON_NUMERIC_CHECK)->send();
        die();
    }

    protected function getAuthenticatedUser()
    {
        $myToken = $this->getToken();
        $usersService = new UsersService($this->modelsManager);
        if (empty($myToken) || $myToken == '') {
            $this->buildErrorResponse('Error', 400, 'common.EMPTY_TOKEN_OR_NOT_RECEIVED');
        }
        try {
            $tokenDecoded = $this->decodeToken($myToken);
            $user = $usersService->get($tokenDecoded->userId);
            if($user == null){
                throw new \Exception('common.USER_NOT_ALLOWED_TO_EDIT_OTHER_USERS_HOUSES', 500);
            }
            return $user;
        } catch (\Throwable $e) {
            $this->buildErrorResponse('Error', 401, 'common.BAD_TOKEN_GET_A_NEW_ONE');
        }
    }

    protected function parseBearerValue($string)
    {
        if (strpos(trim($string), 'Bearer') !== 0) {
            return null;
        }
        return preg_replace('/.*\s/', '', $string);
    }
}
