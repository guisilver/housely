<?php

class IndexController extends DefaultController
{

    private $usersService;

    public function onConstruct()
    {
        $this->usersService = new UsersService($this->modelsManager);
    }

    /**
     * Gets view
     */
    public function index()
    {
        return false;
    }

    /**
     * Public functions
     */
    public function login()
    {
        try {
            $this->initializePost($this->request->getHeaders());
            if ($this->checkIfHeadersExist($this->request->getHeaders())) {
                $user = $this->usersService->findUser($this->request->getBasicAuth());
                $password = $this->getUserPassword($this->request->getBasicAuth());
                $this->checkIfUserIsNotBlocked($user);
                $this->checkIfUserIsAuthorized($user);
                $this->checkPassword($password, $user);

                // ALL OK, proceed to login
                $this->checkIfPasswordNeedsRehash($password, $user);
                $userData = $this->buildUserData($user);
                $token = $this->encodeToken($this->buildTokenData($user));

                $data = array(
                    'token' => $token,
                    'user' => $userData,
                );

                $this->resetLoginAttempts($user);
                $this->buildSuccessResponse(200, 'common.SUCCESSFUL_REQUEST', $data);
            }
        } catch (\Exception $e) {
            $this->buildErrorResponse('Error', $e->getCode(), $e->getMessage());
        }
    }

    /**
     * Private functions
     */
    private function checkIfHeadersExist($headers)
    {
        return (!isset($headers['Authorization']) || empty($headers['Authorization'])) ? $this->buildErrorResponse(403, 'common.HEADER_AUTHORIZATION_NOT_SENT') : true;
    }

    private function getUserPassword($credentials)
    {
        return $credentials['password'];
    }

    private function checkIfUserIsNotBlocked($user)
    {
        $blockExpires = strtotime($user->blockExpires);
        $now = strtotime($this->getNowDateTime());
        return ($blockExpires > $now) ? $this->buildErrorResponse('Error', 403, 'login.USER_BLOCKED') : true;
    }

    private function checkIfUserIsAuthorized($user)
    {
        return ($user->authorised == 0) ? $this->buildErrorResponse('Error', 403, 'login.USER_UNAUTHORIZED') : true;
    }

    private function addOneLoginAttempt($user)
    {
        $user->loginAttempts = $user->loginAttempts + 1;
        $this->usersService->save($user);
        return $user->loginAttempts;
    }

    private function addXMinutesBlockToUser($minutes, $user)
    {
        $user->blockExpires = $this->getNowDateTimePlusMinutes($minutes);
        if ($this->usersService->save($user)) {
            $this->buildErrorResponse('Error', 400, 'login.TOO_MANY_FAILED_loginAttempts');
        }
    }

    private function checkPassword($password, $user)
    {
        if (!password_verify($password, $user->password)) {
            $loginAttempts = $this->addOneLoginAttempt($user);
            ($loginAttempts <= 4) ? $this->buildErrorResponse('Error', 400, 'login.WRONG_USER_PASSWORD') : $this->addXMinutesBlockToUser(120, $user);
        }
    }

    private function checkIfPasswordNeedsRehash($password, $user)
    {
        $options = [
            'cost' => 10, // the default cost is 10, max is 12.
        ];
        if (password_needs_rehash($user->password, PASSWORD_DEFAULT, $options)) {
            $newHash = password_hash($password, PASSWORD_DEFAULT, $options);
            $user->password = $newHash;
            $this->usersService->save($user);
        }
    }

    private function buildUserData($user)
    {
        $userData = array(
            'id' => $user->id,
            'username' => $user->username,
            'firstName' => $user->firstName,
            'lastName' => $user->lastName,
            'role' => $user->role,
        );
        return $userData;
    }

    private function buildTokenData($user)
    {
        // issue at time and expires (token)
        $iat = strtotime($this->getNowDateTime());
        $exp = strtotime('+' . $this->tokenConfig['expiration_time'] . ' seconds', $iat);

        $tokenData = array(
            'iss' => $this->tokenConfig['iss'],
            'aud' => $this->tokenConfig['aud'],
            'iat' => $iat,
            'exp' => $exp,
            'userId' => $user->id,
            'username' => $user->username,
            'firstName' => $user->firstName,
            'lastName' => $user->lastName,
            'role' => $user->role,
            'rand' => rand() . microtime(),
        );
        return $tokenData;
    }

    private function resetLoginAttempts($user)
    {
        $user->loginAttempts = 0;
        $this->usersService->save($user);
    }
}
