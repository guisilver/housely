<?php

class DefaultService
{

    /**
     * Check if there is missing data from the request
     */
    protected function checkForEmptyData($array)
    {
        foreach ($array as $value) {
            if (empty($value)) {
                $this->buildErrorResponse('Error', 400, 'common.INCOMPLETE_DATA_RECEIVED');
            }
        }
    }

}
