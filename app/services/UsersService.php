<?php

use Phalcon\Mvc\Model\Manager;

class UsersService
{

    private $usersRepository;

    /**
     * Public functions
     */
    public function __construct(Manager $modelsManager)
    {
        $this->usersRepository = new UsersRepository($modelsManager);
    }

    public function save($user){
        return $this->usersRepository->save($user);
    }

    public function get($id){
        return $this->usersRepository->get($id);
    }

    public function findUser($credentials)
    {
        $username = $credentials['username'];
        $conditions = 'username = :username:';
        $parameters = array(
            'username' => $username,
        );
        $user = $this->usersRepository->findUser($conditions, $parameters);
        if (!$user) {
            throw new \Exception(404, 'login.USER_IS_NOT_REGISTERED');
        }
        return $user;
    }

}
