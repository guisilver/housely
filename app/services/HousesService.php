<?php

use Phalcon\Mvc\Model\Manager;

class HousesService extends DefaultService
{

    private $housesRepository;

    /**
     * Public functions
     */
    public function __construct(Manager $modelsManager)
    {
        $this->housesRepository = new HousesRepository($modelsManager);
    }

    function list($conditions, $parameters, $orderBy, $offset, $limit) {
        return $this->housesRepository->list($conditions, $parameters, $orderBy, $offset, $limit);
    }

    public function get($id)
    {
        return $this->housesRepository->get($id);
    }

    public function create($data, $user)
    {
        $this->checkForEmptyData(
            [
                $data->city,
                $data->zipCode,
                $data->street,
                $data->number,
            ]
        );
        $this->checksIfHouseAlreadyExists($data);
        $house = new Houses();
        $house->city = trim($data->city);
        $house->zipCode = trim($data->zipCode);
        $house->street = trim($data->street);
        $house->number = trim($data->number);
        $house->addition = trim($data->addition);
        $house->user = $user->id;
        $house->rooms = [];
        if (!empty($data->rooms)) {
            foreach ($data->rooms as $room) {
                $newRoom = new Rooms();
                $newRoom->type = $room->type;
                $newRoom->width = $room->width;
                $newRoom->length = $room->length;
                $newRoom->height = $room->height;
                array_push($house->rooms, $newRoom);
            }
        }
        $house = $this->housesRepository->create($house);
        return $house;
    }

    public function update($id, $data, $user)
    {
        $house = $this->get($id);
        if ($house == null) {
            throw new \Exception('common.HOUSE_NOT_FOUND', 404);
        }
        //Check if user editing owns the house or if user has the credentials to execute the action
        if ($house->user != $user->id && $user->role != Users::USER_ROLE_ADMIN) {
            throw new \Exception('common.USER_NOT_ALLOWED_TO_EDIT_OTHER_USERS_HOUSES', 403);
        }
        $this->checkForEmptyData(
            [
                $id,
                $data->city,
                $data->zipCode,
                $data->street,
                $data->number,
            ]
        );
        $this->checksIfHouseToUpdateAlreadyExists($data);
        $this->checkIfRoomsBelongToHouseOnUpdate($data);
        $house->city = trim($data->city);
        $house->zipCode = trim($data->zipCode);
        $house->street = trim($data->street);
        $house->number = trim($data->number);
        $house->addition = trim($data->addition);
        $house->rooms = [];
        if (!empty($data->rooms)) {
            foreach ($data->rooms as $room) {
                $roomUpdate = new Rooms();
                $roomUpdate->id = $room->id;
                $roomUpdate->house = $house->id;
                $roomUpdate->type = $room->type;
                $roomUpdate->width = $room->width;
                $roomUpdate->length = $room->length;
                $roomUpdate->height = $room->height;
                array_push($house->rooms, $roomUpdate);
            }
        }
        $house = $this->housesRepository->update($house);
        return $house;
    }

    public function delete($id, $user)
    {
        $house = $this->housesRepository->get($id);
        if ($house == null) {
            throw new \Exception('common.HOUSE_NOT_FOUND', 404);
        }
        //Check if user deleting owns the house or if user has the credentials to execute the action
        if ($house->user != $user->id && $user->role != Users::USER_ROLE_ADMIN) {
            throw new \Exception('common.USER_NOT_ALLOWED_TO_DELETE_OTHER_USERS_HOUSES', 403);
        }
        $this->housesRepository->delete($house);
    }

    /**
     * Private functions
     */
    private function checksIfHouseAlreadyExists($data)
    {
        $city = trim($data->city);
        $zipCode = trim($data->zipCode);
        $number = trim($data->number);
        $addition = trim($data->addition);
        $conditions = 'city = :city: AND zipCode = :zipCode: AND number = :number:';
        $parameters = array(
            'city' => $city,
            'zipCode' => $zipCode,
            'number' => $number,
        );
        if (!empty($addition)) {
            $conditions = $conditions . ' AND addition = :addition:';
            $parameters = array_merge($parameters, array('addition' => $addition));
        }

        $orderBy = 'id asc';
        $offset = null;
        $limit = 1;

        $result = $this->housesRepository->list($conditions, $parameters, $orderBy, $offset, $limit);

        if ($result->count() > 0) {
            throw new \Exception('common.THERE_IS_ALREADY_A_RECORD_WITH_THAT_ADDRESS', 409);
        }
    }

    private function checksIfHouseToUpdateAlreadyExists($data)
    {
        $city = trim($data->city);
        $city = trim($data->city);
        $zipCode = trim($data->zipCode);
        $number = trim($data->number);
        $addition = trim($data->addition);
        $conditions = 'city = :city: AND zipCode = :zipCode: AND number = :number: AND id != :id:';
        $parameters = array(
            'city' => $city,
            'zipCode' => $zipCode,
            'number' => $number,
            'id' => $data->id,
        );
        if (!empty($addition)) {
            $conditions = $conditions . ' AND addition = :addition:';
            $parameters = array_merge($parameters, array('addition' => $addition));
        }
        $orderBy = 'id asc';
        $offset = null;
        $limit = 1;

        $result = $this->housesRepository->list($conditions, $parameters, $orderBy, $offset, $limit);

        if ($result->count() > 0) {
            throw new \Exception('common.THERE_IS_ALREADY_A_RECORD_WITH_THAT_ADDRESS', 409);
        }
    }

    private function checkIfRoomsBelongToHouseOnUpdate($data)
    {
        $house = $this->housesRepository->get($data->id);
        foreach ($data->rooms as $room) {
            $roomDb = $this->housesRepository->getRoom($room->id);
            if ($roomDb->house != $house->id) {
                throw new \Exception('common.ROOM_ID_' . $room->id . '_EXISTS_AND_DOESNT_BELONG_TO_THIS_HOUSE', 409);
            }
        }
    }
}
