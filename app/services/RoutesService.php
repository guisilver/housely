<?php

use Phalcon\Mvc\Model\Manager;

class RoutesService
{

    private $routesRepository;

    public function __construct(Manager $modelsManager)
    {
        $this->routesRepository = new RoutesRepository($modelsManager);
    }

    public function getPublicRoutes()
    {
        $parameters = array(
            'private' => 0,
        );
        $routes = $this->routesRepository->getRoutes($parameters);
        return $this->buildRoutes($routes);
    }

    public function getPrivateRoutes()
    {
        $parameters = array(
            'private' => 1,
        );
        $routes = $this->routesRepository->getRoutes($parameters);
        return $this->buildRoutes($routes);
    }

    public function buildRoutes($routes){
        $routesFinal = [];
        $controllers = [];
        foreach($routes as $route){
            if(!in_array($route->controller, $controllers)) {
                array_push($controllers, $route->controller);
            }
        }
        foreach($controllers as $controller){
            $controllerRoutes = [];
            foreach($routes as $route){
                if($route->controller == $controller) {
                    array_push($controllerRoutes, $route->route);
                }
            }
            $routesFinal[$controller] = $controllerRoutes;
        }
        return $routesFinal;
    }
}
