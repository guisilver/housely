<?php

use Phalcon\Mvc\Model\Manager;

class RoutesRepository
{

    protected $modelsManager;

    public function __construct(Manager $modelsManager)
    {
        $this->modelsManager = $modelsManager;
    }

    public function getRoutes($parameters)
    {
        return Routes::find(
            [
                'private = :private:',
                'bind' => $parameters
            ]
        );
    }

}
