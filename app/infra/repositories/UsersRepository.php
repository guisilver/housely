<?php

use Phalcon\Mvc\Model\Manager;

class UsersRepository extends DefaultRepository
{

    public function __construct(Manager $modelsManager)
    {
        parent::__construct($modelsManager);
    }

    public function save($user)
    {
        return $this->tryToSaveData($user);
    }

    public function get($id)
    {
        return $this->findElementById('Users', $id);
    }

    public function findUser($conditions, $parameters)
    {
        return Users::findFirst(
            array(
                $conditions,
                'bind' => $parameters,
            )
        );
    }
}
