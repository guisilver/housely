<?php

use Phalcon\Di;
use Phalcon\DiInterface;
use Phalcon\Mvc\Model\Manager;

class DefaultRepository
{

    protected $modelsManager;

    public function __construct(Manager $modelsManager)
    {
        $this->modelsManager = $modelsManager;
    }

    /**
     * Find element by ID from an specified model
     */
    public function findElementById($model, $id)
    {
        $conditions = 'id = :id:';
        $parameters = array(
            'id' => $id,
        );
        $element = $model::findFirst(
            array(
                $conditions,
                'bind' => $parameters,
            )
        );
        return $element;
    }

    /**
     * Find elements from an specified model
     */
    public function findElements($model, $conditions, $parameters, $columns, $orderBy, $offset, $limit)
    {
        $elements = $model::find(
            array(
                $conditions,
                'bind' => $parameters,
                'columns' => $columns,
                'order' => $orderBy,
                'offset' => $offset,
                'limit' => $limit,
            )
        );
        return $elements;
    }

    /**
     * Find elements from an specified model
     */
    public function findElementsByQuery($query, $conditions, $parameters, $orderBy, $offset, $limit)
    {
        if (!empty($conditions)) {
            $query = $query . ' AND ' . $conditions;
        }
        if (!empty($orderBy)) {
            $query = $query . ' ORDER BY ' . $orderBy;
        }

        if (!empty($limit)) {
            $query = $query . ' LIMIT ' . $limit;
        }

        if (!empty($offset)) {
            $query = $query . ' OFFSET ' . $offset;
        }

        $query = $this->modelsManager->createQuery($query);
        $elements = $query->execute($parameters);
        return $elements;
    }

    /**
     * Try to save data in DB
     */
    public function tryToSaveData($element, $customMessage = 'common.THERE_HAS_BEEN_AN_ERROR')
    {
        if (!$element->save()) {
            // Send errors
            $errors = array();
            foreach ($element->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
        }
        return true;
    }

    /**
     * Try to delete data in DB
     */
    public function tryToDeleteData($element)
    {
        if (!$element->delete()) {
            // Send errors
            $errors = array();
            foreach ($element->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
        }
        return true;
    }
}
