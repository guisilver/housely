<?php

use Phalcon\Mvc\Model\Manager;

class HousesRepository extends DefaultRepository
{

    public function __construct(Manager $modelsManager)
    {
        parent::__construct($modelsManager);
    }

    public function list($conditions, $parameters, $orderBy, $offset, $limit) 
    {
        $query = "SELECT * FROM Houses WHERE 1 = 1";
        if (!empty($parameters['minimalBedroomsCount'])) {
            $query .= " AND (SELECT COUNT(*) FROM Rooms AS r WHERE r.house = Houses.id and r.type = 2) >= :minimalBedroomsCount:";
        }
        if (!empty($parameters['minimalToiletCount'])) {
            $query .= " AND (SELECT COUNT(*) FROM Rooms AS r WHERE r.house = Houses.id and r.type = 3) >= :minimalToiletCount:";
        }
        if (!empty($parameters['search'])) {
            $query .= " AND (
                Houses.id like :search: OR 
                Houses.city like :search: OR 
                Houses.zipCode like :search: OR 
                Houses.street like :search: OR 
                Houses.number like :search: OR 
                Houses.addition like :search:) ";
        }
        return $this->findElementsByQuery(
            $query,
            $conditions,
            $parameters,
            $orderBy,
            $offset,
            $limit,
        );
    }

    public function get($id)
    {
        return $this->findElementById('Houses', $id);
    }

    public function getRoom($id)
    {
        return $this->findElementById('Rooms', $id);
    }

    public function create($house)
    {
        $this->tryToSaveData($house, 'common.COULD_NOT_BE_CREATED');
        if (!empty($house->rooms)) {
            foreach ($house->rooms as $room) {
                $room->house = $house->id;
                $this->tryToSaveData($room, 'common.COULD_NOT_BE_CREATED');
            }
        }
        return $house;
    }

    public function update($house)
    {
        $newRooms = $house->rooms;
        $this->tryToSaveData($house, 'common.COULD_NOT_BE_UPDATED');
        if (!empty($house->rooms)) {
            foreach ($house->rooms as $room) {
                $this->tryToSaveData($room, 'common.COULD_NOT_BE_UPDATED');
            }
        }
        //Delete rooms in the database that were removed from the JSON
        $dbRooms = $house->getRooms();
        $dbRoomsId = (function () use ($dbRooms) {
            $result = [];
            foreach ($dbRooms as $roomDb) {
                array_push($result, $roomDb->id);
            }
            return $result;
        })();
        $newRoomsId = (function () use ($newRooms) {
            $result = [];
            foreach ($newRooms as $room) {
                array_push($result, $room->id);
            }
            return $result;
        })();
        foreach ($dbRoomsId as $dbRoomId) {
            if (!in_array($dbRoomId, $newRoomsId)) {
                $roomToDelete = $this->findElementById("Rooms", $dbRoomId);
                $this->tryToDeleteData($roomToDelete);
            }
        }
        return $house;
    }

    public function delete($house)
    {
        foreach ($house->getRooms() as $room) {
            $roomToDelete = $this->findElementById("Rooms", $room->id);
            $this->tryToDeleteData($roomToDelete);
        }
        $this->tryToDeleteData($house);
    }

}
