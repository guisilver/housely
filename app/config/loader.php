<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->servicesDir,
        $config->application->repositoriesDir,
        $config->application->middlewaresDir,
        $config->application->modelsDir,
    )
)->register();
