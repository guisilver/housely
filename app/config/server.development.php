<?php

return [
    'database' => [
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'fodase01',
        'dbname' => 'microapi',
        'charset' => 'utf8',
    ],
    'authentication' => [
        'secret' => 'H0p3!', // This will sign the token. (still insecure)
        'encryption_key' => 'f3K@#genf3&tZ5!P2', // Secure token with an ultra password
        'expiration_time' => 86400 * 7, // One week till token expires
        'iss' => 'myproject', // Token issuer eg. www.myproject.com
        'aud' => 'myproject', // Token audience eg. www.myproject.com
    ],
];
