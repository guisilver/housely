<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));
defined('APPLICATION_ENV') || define('APPLICATION_ENV', 'development');

$config = new \Phalcon\Config([
    'application' => [
        'title' => 'API REST',
        'description' => 'API REST',
        'controllersDir' => APP_PATH . '/app/controllers/',
        'servicesDir' => APP_PATH . '/app/services/',
        'repositoriesDir' => APP_PATH . '/app/infra/repositories/',
        'middlewaresDir' => APP_PATH . '/app/middlewares/',
        'modelsDir' => APP_PATH . '/app/models/',
        'migrationsDir' => APP_PATH . '/migrations/',
        'baseUri' => '/',
    ],
]);

$configOverride = new \Phalcon\Config(include_once __DIR__ . '/../config/server.' . APPLICATION_ENV . '.php');

$config = $config->merge($configOverride);

return $config;
