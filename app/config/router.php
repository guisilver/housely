<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */
use Phalcon\Mvc\Micro\Collection as MicroCollection;

/**
 * ACL checks
 */
$app->before(new AccessMiddleware());

/**
 * Index
 */
$index = new MicroCollection();
$index->setHandler('IndexController', true);
// Gets index
$index->get('/', 'index');
// Authenticates a user
$index->post('/authenticate', 'login');
// Adds index routes to $app
$app->mount($index);

/**
 * Houses
 */
$houses = new MicroCollection();
$houses->setHandler('HousesController', true);
$houses->setPrefix('/houses');
// Gets houses
$houses->get('/', 'list');
// Creates a new house
$houses->post('/create', 'create');
// Gets house based on unique key
$houses->get('/get/{id}', 'get');
// Updates house based on unique key
$houses->patch('/update/{id}', 'update');
// Deletes house based on unique key
$houses->delete('/delete/{id}', 'delete');
// Adds houses routes to $app
$app->mount($houses);

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404)->sendHeaders();
    $app->response->setContentType('application/json', 'UTF-8');
    $app->response->setJsonContent(array(
        'code' => '404',
        'message' => 'Not Found',
    ));
    $app->response->send();
});

/**
 * Error handler
 */
$app->error(
    function ($exception) {
        print_r('An error has occurred');
    }
);
