<?php

$acl = new Phalcon\Acl\Adapter\Memory();

// The default action is DENY access
$acl->setDefaultAction(Phalcon\Acl\Enum::DENY);

/*
 * ROLES
 * Admin - can do anything (Guest, User, and own things)
 * User - can do most things (Guest and own things)
 * Guest - Public
 * */
$acl->addRole(new Phalcon\Acl\Role('Guest'));
$acl->addRole(new Phalcon\Acl\Role('User'));
$acl->addRole(new Phalcon\Acl\Role('Admin'));


// User can do everything a Guest can do
$acl->addInherit('User', 'Guest');
// // Admin can do everything a User can do
$acl->addInherit('Admin', 'Guest');
$acl->addInherit('Admin', 'User');

/*
 * RESOURCES
 * for each user, specify the 'controller' and 'method' they have access to ('userType'=>['controller'=>['method','method','...']],...)
 * this is created in an array as we later loop over this structure to assign users to resources
 * */
$publicRoutes = $this->routesService->getPublicRoutes();
$privateRoutes = $this->routesService->getPrivateRoutes();

$arrResources = [
    'Guest' => $publicRoutes,
    'User' => $privateRoutes,
    'Admin' => $privateRoutes,
];

foreach ($arrResources as $arrResource) {
    foreach ($arrResource as $controller => $arrMethods) {
        $acl->addComponent(new Phalcon\Acl\Component($controller), $arrMethods);
    }
}

/*
 * ACCESS
 * */
foreach ($acl->getRoles() as $objRole) {
    $roleName = $objRole->getName();

    // everyone gets access to global resources
    foreach ($arrResources['Guest'] as $resource => $method) {
        $acl->allow($roleName, $resource, $method);
    }

    // users
    if ($roleName == 'User') {
        foreach ($arrResources['User'] as $resource => $method) {
            $acl->allow($roleName, $resource, $method);
        }
    }

    // admins
    if ($roleName == 'Admin') {
        foreach ($arrResources['Admin'] as $resource => $method) {
            $acl->allow($roleName, $resource, $method);
        }
    }
}
