<?php

use Phalcon\Validation;

class Users extends \Phalcon\Mvc\Model
{

    const USER_ROLE_ADMIN = 'Admin';
    const USER_ROLE_USER = 'User';
    const USER_ROLE_GUEST = 'Guest';

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $firstName;

    /**
     *
     * @var string
     */
    public $lastName;

    /**
     *
     * @var string
     */
    public $role;

    /**
     *
     * @var integer
     */
    public $authorised;

    /**
     *
     * @var string
     */
    public $blockExpires;

    /**
     *
     * @var integer
     */
    public $loginAttempts;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("microapi");
        $this->setSource("users");
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();
        return $this->validate($validator);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'username' => 'username',
            'password' => 'password',
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'role' => 'role',
            'authorised' => 'authorised',
            'blockExpires' => 'blockExpires',
            'loginAttempts' => 'loginAttempts',
        );
    }

}
