<?php

class Rooms extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $type;

    /**
     *
     * @var integer
     */
    public $house;

    /**
     *
     * @var integer
     */
    public $width;

    /**
     *
     * @var integer
     */
    public $length;

    /**
     *
     * @var integer
     */
    public $height;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("microapi");
        $this->setSource("rooms");
        $this->belongsTo('house', 'Houses', 'id', ['alias' => 'houses']);
        $this->belongsTo('type', RoomTypes::class, 'id', ['alias' => 'type']);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'type' => 'type',
            'house' => 'house',
            'width' => 'width',
            'length' => 'length',
            'height' => 'height',
        );
    }

    public function getType($arguments = null) {
        return $this->getRelated('type', $arguments);
    }
}
