<?php

class Houses extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $city;

    /**
     *
     * @var string
     */
    public $zipCode;

    /**
     *
     * @var string
     */
    public $street;

    /**
     *
     * @var integer
     */
    public $number;

    /**
     *
     * @var string
     */
    public $addition;

    /**
     *
     * @var integer
     */
    public $user;

    public $rooms = [];

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("microapi");
        $this->setSource("houses");
        $this->hasMany('id', Rooms::class, 'house', ['alias' => 'rooms']);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'city' => 'city',
            'zipCode' => 'zipCode',
            'street' => 'street',
            'number' => 'number',
            'addition' => 'addition',
            'user' => 'user',
        );
    }

    public function getRooms($arguments = null) {
        return $this->getRelated('rooms', $arguments);
    }

    public function toArray($columns = null) : array {
        $output = parent::toArray($columns);
        $output['rooms'] = $this->getRooms([
            'columns' => [
                'id',
                'type',
                'house',
                'width',
                'length',
                'height'
            ]
        ])->toArray();
        return $output;
    }
}
