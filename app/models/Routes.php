<?php

class Routes extends \Phalcon\Mvc\Model
{


    /**
     *
     * @var string
     */
    public $controller;

    /**
     *
     * @var string
     */
    public $route;

    /**
     *
     * @var bool
     */
    public $private;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("microapi");
        $this->setSource("routes");
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'controller' => 'controller',
            'route' => 'route',
            'private' => 'private',
        );
    }
}
