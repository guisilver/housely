<?php

class RoomTypes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $type;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("microapi");
        $this->setSource("room_types");
        $this->hasMany('id', Rooms::class, 'type', ['alias' => 'rooms']);
    }
}
