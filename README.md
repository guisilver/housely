How to install
--------------

1. First you need to [install composer](https://getcomposer.org/download/) if you haven´t already.
2. Clone the project from bitbucket.
```bash
git clone https://guisilver@bitbucket.org/guisilver/housely.git
```

### Install composer dependencies

```bash
cd housely
composer install
```

### Database Configuration and Security

1. Create a MySQL database with your custom name and then import `dump.sql` (in the `/schemas` directory)
3. Open `/housely/config/server.development.php` and setup your DEVELOPMENT (local) database connection credentials


Usage
--------------

You can run the project by typing the folowing command inside the project folder:
```bash
phalcon serve
```

### Testing

The routes available are:

Access:

Authenticates a user - Method: Post

```bash
http://localhost:8000/authenticate
```

Houses:

Gets all houses - Method: Get
```bash
http://localhost:8000/houses/
```

Creates a new house - Method: Post
```bash
http://localhost:8000/houses/create
```

Gets house based on unique key - Method: Get
```bash
http://localhost:8000/houses/get/{id}
```

Updates house based on unique key - Method: Patch
```bash
http://localhost:8000/houses/update/{id}
```

Deletes house based on unique key - Method: Delete
```bash
http://localhost:8000/houses/delete/{id}
```


