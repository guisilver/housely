-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: microapi
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `houses`
--

DROP TABLE IF EXISTS `houses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `houses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `street` varchar(100) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `addition` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `user` int(10) unsigned DEFAULT NULL,
  `zipCode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_house` (`zipCode`,`number`),
  KEY `user` (`user`),
  CONSTRAINT `houses_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `houses`
--

LOCK TABLES `houses` WRITE;
/*!40000 ALTER TABLE `houses` DISABLE KEYS */;
INSERT INTO `houses` VALUES (1,'Bentinckstraat',1,'Updated11','Lichtenvoorde',2,'7131AW'),(3,'Bentinckstraat',3,NULL,'Lichtenvoorde',2,'7131AW'),(4,'Bentinckstraat',4,NULL,'Lichtenvoorde',2,'7131AW'),(5,'Bentinckstraat',5,NULL,'Lichtenvoorde',2,'7131AW'),(6,'Bentinckstraat',6,NULL,'Lichtenvoorde',2,'7131AW'),(7,'Bentinckstraat',7,NULL,'Lichtenvoorde',2,'7131AW'),(8,'Bentinckstraat',8,NULL,'Lichtenvoorde',2,'7131AW'),(9,'Bentinckstraat',9,NULL,'Lichtenvoorde',2,'7131AW'),(10,'Bentinckstraat',10,NULL,'Lichtenvoorde',2,'7131AW'),(11,'Bentinckstraat',11,NULL,'Lichtenvoorde',2,'7131AW'),(12,'Bentinckstraat',12,NULL,'Lichtenvoorde',2,'7131AW'),(13,'Bentinckstraat',13,NULL,'Lichtenvoorde',2,'7131AW'),(14,'Bentinckstraat',14,NULL,'Lichtenvoorde',2,'7131AW'),(15,'Bentinckstraat',15,NULL,'Lichtenvoorde',2,'7131AW'),(16,'Bentinckstraat',16,NULL,'Lichtenvoorde',2,'7131AW'),(17,'Bentinckstraat',17,NULL,'Lichtenvoorde',2,'7131AW'),(18,'Bentinckstraat',18,NULL,'Lichtenvoorde',2,'7131AW'),(19,'Bentinckstraat',19,NULL,'Lichtenvoorde',2,'7131AW'),(20,'Bentinckstraat',20,NULL,'Lichtenvoorde',2,'7131AW'),(21,'Bentinckstraat',21,NULL,'Lichtenvoorde',2,'7131AW'),(22,'Bentinckstraat',22,NULL,'Lichtenvoorde',2,'7131AW'),(23,'Bentinckstraat',23,NULL,'Lichtenvoorde',2,'7131AW'),(24,'Bentinckstraat',24,NULL,'Lichtenvoorde',2,'7131AW'),(25,'Bentinckstraat',25,NULL,'Lichtenvoorde',2,'7131AW'),(26,'Bentinckstraat',26,NULL,'Lichtenvoorde',2,'7131AW'),(27,'Bentinckstraat',27,NULL,'Lichtenvoorde',2,'7131AW'),(28,'Bentinckstraat',28,NULL,'Lichtenvoorde',2,'7131AW'),(29,'Bentinckstraat',29,NULL,'Lichtenvoorde',2,'7131AW'),(30,'Bentinckstraat',30,NULL,'Lichtenvoorde',2,'7131AW'),(31,'Frans Ten Bosch Straat',1,NULL,'Lichtenvoorde',4,'7131ZW'),(32,'Frans Ten Bosch Straat',2,NULL,'Lichtenvoorde',4,'7131ZW'),(33,'Frans Ten Bosch Straat',3,NULL,'Lichtenvoorde',4,'7131ZW'),(34,'Frans Ten Bosch Straat',4,NULL,'Lichtenvoorde',4,'7131ZW'),(35,'Frans Ten Bosch Straat',5,NULL,'Lichtenvoorde',4,'7131ZW'),(36,'Frans Ten Bosch Straat',6,NULL,'Lichtenvoorde',4,'7131ZW'),(37,'Frans Ten Bosch Straat',7,NULL,'Lichtenvoorde',4,'7131ZW'),(38,'Frans Ten Bosch Straat',8,NULL,'Lichtenvoorde',4,'7131ZW'),(39,'Frans Ten Bosch Straat',9,NULL,'Lichtenvoorde',4,'7131ZW'),(40,'Frans Ten Bosch Straat',10,NULL,'Lichtenvoorde',4,'7131ZW'),(41,'Frans Ten Bosch Straat',11,NULL,'Lichtenvoorde',4,'7131ZW'),(42,'Frans Ten Bosch Straat',12,NULL,'Lichtenvoorde',4,'7131ZW'),(43,'Frans Ten Bosch Straat',13,NULL,'Lichtenvoorde',4,'7131ZW'),(44,'Frans Ten Bosch Straat',14,NULL,'Lichtenvoorde',4,'7131ZW'),(45,'Frans Ten Bosch Straat',15,NULL,'Lichtenvoorde',4,'7131ZW'),(46,'Frans Ten Bosch Straat',16,NULL,'Lichtenvoorde',4,'7131ZW'),(47,'Frans Ten Bosch Straat',17,NULL,'Lichtenvoorde',4,'7131ZW'),(48,'Frans Ten Bosch Straat',18,NULL,'Lichtenvoorde',4,'7131ZW'),(49,'Frans Ten Bosch Straat',19,NULL,'Lichtenvoorde',4,'7131ZW'),(50,'Frans Ten Bosch Straat',20,NULL,'Lichtenvoorde',4,'7131ZW'),(51,'Frans Ten Bosch Straat',21,NULL,'Lichtenvoorde',4,'7131ZW'),(52,'Frans Ten Bosch Straat',22,NULL,'Lichtenvoorde',4,'7131ZW'),(53,'Frans Ten Bosch Straat',23,NULL,'Lichtenvoorde',4,'7131ZW'),(54,'Frans Ten Bosch Straat',24,NULL,'Lichtenvoorde',4,'7131ZW'),(55,'Frans Ten Bosch Straat',25,NULL,'Lichtenvoorde',4,'7131ZW'),(56,'Frans Ten Bosch Straat',26,NULL,'Lichtenvoorde',4,'7131ZW'),(57,'Frans Ten Bosch Straat',27,NULL,'Lichtenvoorde',4,'7131ZW'),(58,'Frans Ten Bosch Straat',28,NULL,'Lichtenvoorde',4,'7131ZW'),(59,'Frans Ten Bosch Straat',29,NULL,'Lichtenvoorde',4,'7131ZW'),(60,'Frans Ten Bosch Straat',30,NULL,'Lichtenvoorde',4,'7131ZW'),(61,'Eelinkstraat',1,NULL,'Winterswijk',2,'7101JJ'),(62,'Eelinkstraat',2,NULL,'Winterswijk',2,'7101JJ'),(63,'Eelinkstraat',3,NULL,'Winterswijk',2,'7101JJ'),(64,'Eelinkstraat',4,NULL,'Winterswijk',2,'7101JJ'),(65,'Eelinkstraat',5,NULL,'Winterswijk',2,'7101JJ'),(66,'Eelinkstraat',6,NULL,'Winterswijk',2,'7101JJ'),(67,'Eelinkstraat',7,NULL,'Winterswijk',2,'7101JJ'),(68,'Eelinkstraat',8,NULL,'Winterswijk',2,'7101JJ'),(69,'Eelinkstraat',9,NULL,'Winterswijk',2,'7101JJ'),(70,'Eelinkstraat',10,NULL,'Winterswijk',2,'7101JJ'),(71,'Eelinkstraat',11,NULL,'Winterswijk',2,'7101JJ'),(72,'Eelinkstraat',12,NULL,'Winterswijk',2,'7101JJ'),(73,'Eelinkstraat',13,NULL,'Winterswijk',2,'7101JJ'),(74,'Eelinkstraat',14,NULL,'Winterswijk',2,'7101JJ'),(75,'Eelinkstraat',15,NULL,'Winterswijk',2,'7101JJ'),(76,'Eelinkstraat',16,NULL,'Winterswijk',2,'7101JJ'),(77,'Eelinkstraat',17,NULL,'Winterswijk',2,'7101JJ'),(78,'Eelinkstraat',18,NULL,'Winterswijk',2,'7101JJ'),(79,'Eelinkstraat',19,NULL,'Winterswijk',2,'7101JJ'),(80,'Eelinkstraat',20,NULL,'Winterswijk',2,'7101JJ'),(81,'Eelinkstraat',21,NULL,'Winterswijk',2,'7101JJ'),(82,'Eelinkstraat',22,NULL,'Winterswijk',2,'7101JJ'),(83,'Eelinkstraat',23,NULL,'Winterswijk',2,'7101JJ'),(84,'Eelinkstraat',24,NULL,'Winterswijk',2,'7101JJ'),(85,'Eelinkstraat',25,NULL,'Winterswijk',2,'7101JJ'),(86,'Eelinkstraat',26,NULL,'Winterswijk',2,'7101JJ'),(87,'Eelinkstraat',27,NULL,'Winterswijk',2,'7101JJ'),(88,'Eelinkstraat',28,NULL,'Winterswijk',2,'7101JJ'),(89,'Eelinkstraat',29,NULL,'Winterswijk',2,'7101JJ'),(90,'Eelinkstraat',30,NULL,'Winterswijk',2,'7101JJ'),(91,'Bredevoortsestraatweg',1,NULL,'Aalten',2,'7121BB'),(92,'Bredevoortsestraatweg',2,NULL,'Aalten',2,'7121BB'),(93,'Bredevoortsestraatweg',3,NULL,'Aalten',2,'7121BB'),(94,'Bredevoortsestraatweg',4,NULL,'Aalten',2,'7121BB'),(95,'Bredevoortsestraatweg',5,NULL,'Aalten',2,'7121BB'),(96,'Bredevoortsestraatweg',6,NULL,'Aalten',2,'7121BB'),(97,'Bredevoortsestraatweg',7,NULL,'Aalten',2,'7121BB'),(98,'Bredevoortsestraatweg',8,NULL,'Aalten',2,'7121BB'),(99,'Bredevoortsestraatweg',9,NULL,'Aalten',2,'7121BB'),(100,'Bredevoortsestraatweg',10,NULL,'Aalten',2,'7121BB'),(101,'Bredevoortsestraatweg',11,NULL,'Aalten',2,'7121BB'),(102,'Bredevoortsestraatweg',12,NULL,'Aalten',2,'7121BB'),(103,'Bredevoortsestraatweg',13,NULL,'Aalten',2,'7121BB'),(104,'Bredevoortsestraatweg',14,NULL,'Aalten',2,'7121BB'),(105,'Bredevoortsestraatweg',15,NULL,'Aalten',2,'7121BB'),(106,'Bredevoortsestraatweg',16,NULL,'Aalten',2,'7121BB'),(107,'Bredevoortsestraatweg',17,NULL,'Aalten',2,'7121BB'),(108,'Bredevoortsestraatweg',18,NULL,'Aalten',2,'7121BB'),(109,'Bredevoortsestraatweg',19,NULL,'Aalten',2,'7121BB'),(110,'Bredevoortsestraatweg',20,NULL,'Aalten',2,'7121BB'),(111,'Bredevoortsestraatweg',21,NULL,'Aalten',2,'7121BB'),(112,'Bredevoortsestraatweg',22,NULL,'Aalten',2,'7121BB'),(113,'Bredevoortsestraatweg',23,NULL,'Aalten',2,'7121BB'),(114,'Bredevoortsestraatweg',24,NULL,'Aalten',2,'7121BB'),(115,'Bredevoortsestraatweg',25,NULL,'Aalten',2,'7121BB'),(116,'Bredevoortsestraatweg',26,NULL,'Aalten',2,'7121BB'),(117,'Bredevoortsestraatweg',27,NULL,'Aalten',2,'7121BB'),(118,'Bredevoortsestraatweg',28,NULL,'Aalten',2,'7121BB'),(119,'Bredevoortsestraatweg',29,NULL,'Aalten',2,'7121BB'),(120,'Bredevoortsestraatweg',30,NULL,'Aalten',2,'7121BB'),(121,'Wevelgemstraat',1,NULL,'Amsterdam',5,'1066TM'),(122,'Wevelgemstraat',2,NULL,'Amsterdam',5,'1066TM'),(123,'Wevelgemstraat',3,NULL,'Amsterdam',5,'1066TM'),(124,'Wevelgemstraat',4,NULL,'Amsterdam',5,'1066TM'),(125,'Wevelgemstraat',5,NULL,'Amsterdam',5,'1066TM'),(126,'Wevelgemstraat',6,NULL,'Amsterdam',5,'1066TM'),(127,'Wevelgemstraat',7,NULL,'Amsterdam',5,'1066TM'),(128,'Wevelgemstraat',8,NULL,'Amsterdam',5,'1066TM'),(129,'Wevelgemstraat',9,NULL,'Amsterdam',5,'1066TM'),(130,'Wevelgemstraat',10,NULL,'Amsterdam',5,'1066TM'),(131,'Wevelgemstraat',11,NULL,'Amsterdam',5,'1066TM'),(132,'Wevelgemstraat',12,NULL,'Amsterdam',5,'1066TM'),(133,'Wevelgemstraat',13,NULL,'Amsterdam',5,'1066TM'),(134,'Wevelgemstraat',14,NULL,'Amsterdam',5,'1066TM'),(135,'Wevelgemstraat',15,NULL,'Amsterdam',5,'1066TM'),(136,'Wevelgemstraat',16,NULL,'Amsterdam',5,'1066TM'),(137,'Wevelgemstraat',17,NULL,'Amsterdam',5,'1066TM'),(138,'Wevelgemstraat',18,NULL,'Amsterdam',5,'1066TM'),(139,'Wevelgemstraat',19,NULL,'Amsterdam',5,'1066TM'),(140,'Wevelgemstraat',20,NULL,'Amsterdam',5,'1066TM'),(141,'Wevelgemstraat',21,NULL,'Amsterdam',5,'1066TM'),(142,'Wevelgemstraat',22,NULL,'Amsterdam',5,'1066TM'),(143,'Wevelgemstraat',23,NULL,'Amsterdam',5,'1066TM'),(144,'Wevelgemstraat',24,NULL,'Amsterdam',5,'1066TM'),(145,'Wevelgemstraat',25,NULL,'Amsterdam',5,'1066TM'),(146,'Wevelgemstraat',26,NULL,'Amsterdam',5,'1066TM'),(147,'Wevelgemstraat',27,NULL,'Amsterdam',5,'1066TM'),(148,'Wevelgemstraat',28,NULL,'Amsterdam',5,'1066TM'),(213,'Bentinckstraat',31,'','Lichtenvoorde',1,'7131AW'),(215,'Bentinckstraat',32,'','Lichtenvoorde',1,'7131AW'),(217,'Bentinckstraat',33,'','Lichtenvoorde',1,'7131AW'),(219,'Bentinckstraat',500,'','Lichtenvoorde',1,'7131AW'),(221,'Bentinckstraat',851,'','Lichtenvoorde',1,'7131AW'),(230,'Bentinckstraat',44,'','Lichtenvoorde',1,'7131AW'),(233,'Bentinckstraat',106,'','Lichtenvoorde',1,'7131AW'),(236,'Bentinckstraat',965,'','Lichtenvoorde',1,'7131AW'),(290,'Bentinckstraat',123222,'','Lichtenvoorde',1,'7131AW'),(291,'Bentinckstraat',24242423,'','Lichtenvoorde',1,'7131AW'),(292,'Bentinckstraat',242453,'','Lichtenvoorde',1,'7131AW'),(297,'Bentinckstraat',77777,'','Lichtenvoorde',1,'7131AW'),(298,'Bentinckstraat',1234567,'','Lichtenvoorde',1,'7131AW'),(299,'Bentinckstraat',123456,'','Lichtenvoorde',1,'7131AW');
/*!40000 ALTER TABLE `houses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_types`
--

LOCK TABLES `room_types` WRITE;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` VALUES (1,'living room'),(2,'bedroom'),(3,'toilet'),(4,'storage'),(5,'bathroom');
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned DEFAULT NULL,
  `house` int(10) unsigned DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_room_type` (`type`),
  KEY `fk_house` (`house`),
  CONSTRAINT `fk_house` FOREIGN KEY (`house`) REFERENCES `houses` (`id`),
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`type`) REFERENCES `room_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,4,1,2000,1500,1500),(33,2,3,2000,1500,1500),(34,2,3,2000,1500,1500),(35,4,3,2000,1500,1500),(36,3,3,2000,1500,1500),(37,3,4,2000,1500,1500),(38,3,4,2000,1500,1500),(39,4,4,2000,1500,1500),(44,4,3,2000,1500,1500),(45,2,3,2000,1500,1500),(46,4,3,2000,1500,1500),(47,3,4,2000,1500,1500),(48,3,4,2000,1500,1500),(49,4,4,2000,1500,1500),(50,1,4,2000,1500,1500),(51,4,5,2000,1500,1500),(52,1,5,2000,1500,1500),(53,3,6,2000,1500,1500),(54,5,6,2000,1500,1500),(55,2,6,2000,1500,1500),(56,4,7,2000,1500,1500),(57,4,7,2000,1500,1500),(58,5,7,2000,1500,1500),(59,5,8,2000,1500,1500),(60,2,8,2000,1500,1500),(61,3,8,2000,1500,1500),(62,3,9,2000,1500,1500),(63,3,10,2000,1500,1500),(64,3,10,2000,1500,1500),(65,2,11,2000,1500,1500),(66,3,12,2000,1500,1500),(67,4,12,2000,1500,1500),(68,1,12,2000,1500,1500),(69,2,12,2000,1500,1500),(70,5,13,2000,1500,1500),(71,1,13,2000,1500,1500),(72,4,13,2000,1500,1500),(73,1,13,2000,1500,1500),(74,3,14,2000,1500,1500),(75,2,15,2000,1500,1500),(76,4,15,2000,1500,1500),(77,3,15,2000,1500,1500),(78,3,16,2000,1500,1500),(79,2,17,2000,1500,1500),(80,4,17,2000,1500,1500),(81,3,18,2000,1500,1500),(82,2,18,2000,1500,1500),(83,3,18,2000,1500,1500),(84,2,18,2000,1500,1500),(85,2,19,2000,1500,1500),(86,4,19,2000,1500,1500),(87,2,19,2000,1500,1500),(88,5,20,2000,1500,1500),(89,3,20,2000,1500,1500),(90,3,20,2000,1500,1500),(91,2,21,2000,1500,1500),(92,2,21,2000,1500,1500),(93,2,21,2000,1500,1500),(94,3,21,2000,1500,1500),(95,3,22,2000,1500,1500),(96,3,22,2000,1500,1500),(97,4,22,2000,1500,1500),(98,3,23,2000,1500,1500),(99,1,23,2000,1500,1500),(100,3,24,2000,1500,1500),(101,3,24,2000,1500,1500),(102,5,25,2000,1500,1500),(103,1,25,2000,1500,1500),(104,2,25,2000,1500,1500),(105,3,25,2000,1500,1500),(106,4,26,2000,1500,1500),(107,4,26,2000,1500,1500),(108,4,26,2000,1500,1500),(109,2,27,2000,1500,1500),(110,5,27,2000,1500,1500),(111,3,27,2000,1500,1500),(112,4,28,2000,1500,1500),(113,1,28,2000,1500,1500),(114,3,29,2000,1500,1500),(115,2,29,2000,1500,1500),(116,2,29,2000,1500,1500),(117,3,30,2000,1500,1500),(118,1,31,2000,1500,1500),(119,4,32,2000,1500,1500),(120,3,32,2000,1500,1500),(121,1,33,2000,1500,1500),(122,3,33,2000,1500,1500),(123,2,34,2000,1500,1500),(124,4,34,2000,1500,1500),(125,2,34,2000,1500,1500),(126,2,35,2000,1500,1500),(127,3,36,2000,1500,1500),(128,3,36,2000,1500,1500),(129,4,36,2000,1500,1500),(130,3,37,2000,1500,1500),(131,3,37,2000,1500,1500),(132,4,37,2000,1500,1500),(133,5,38,2000,1500,1500),(134,2,38,2000,1500,1500),(135,4,39,2000,1500,1500),(136,5,39,2000,1500,1500),(137,5,40,2000,1500,1500),(138,4,40,2000,1500,1500),(139,2,41,2000,1500,1500),(140,3,41,2000,1500,1500),(141,2,41,2000,1500,1500),(142,3,41,2000,1500,1500),(143,4,42,2000,1500,1500),(144,3,42,2000,1500,1500),(145,1,42,2000,1500,1500),(146,2,42,2000,1500,1500),(147,3,43,2000,1500,1500),(148,2,44,2000,1500,1500),(149,1,44,2000,1500,1500),(150,3,44,2000,1500,1500),(151,4,44,2000,1500,1500),(152,2,45,2000,1500,1500),(153,2,46,2000,1500,1500),(154,2,46,2000,1500,1500),(155,2,46,2000,1500,1500),(156,2,47,2000,1500,1500),(157,4,47,2000,1500,1500),(158,3,48,2000,1500,1500),(159,4,48,2000,1500,1500),(160,2,48,2000,1500,1500),(161,1,48,2000,1500,1500),(162,2,49,2000,1500,1500),(163,2,49,2000,1500,1500),(164,1,49,2000,1500,1500),(165,1,50,2000,1500,1500),(166,3,50,2000,1500,1500),(167,4,50,2000,1500,1500),(168,2,51,2000,1500,1500),(169,4,51,2000,1500,1500),(170,1,51,2000,1500,1500),(171,1,52,2000,1500,1500),(172,2,53,2000,1500,1500),(173,4,54,2000,1500,1500),(174,2,54,2000,1500,1500),(175,3,55,2000,1500,1500),(176,1,55,2000,1500,1500),(177,2,55,2000,1500,1500),(178,2,56,2000,1500,1500),(179,2,57,2000,1500,1500),(180,4,57,2000,1500,1500),(181,3,58,2000,1500,1500),(182,5,58,2000,1500,1500),(183,4,59,2000,1500,1500),(184,4,60,2000,1500,1500),(185,4,60,2000,1500,1500),(186,1,60,2000,1500,1500),(187,1,61,2000,1500,1500),(188,2,61,2000,1500,1500),(189,2,61,2000,1500,1500),(190,4,62,2000,1500,1500),(191,3,63,2000,1500,1500),(192,4,64,2000,1500,1500),(193,2,64,2000,1500,1500),(194,3,65,2000,1500,1500),(195,4,66,2000,1500,1500),(196,3,67,2000,1500,1500),(197,1,67,2000,1500,1500),(198,2,67,2000,1500,1500),(199,3,67,2000,1500,1500),(200,5,68,2000,1500,1500),(201,3,69,2000,1500,1500),(202,2,69,2000,1500,1500),(203,2,70,2000,1500,1500),(204,2,71,2000,1500,1500),(205,5,71,2000,1500,1500),(206,5,71,2000,1500,1500),(207,2,71,2000,1500,1500),(208,3,72,2000,1500,1500),(209,2,72,2000,1500,1500),(210,2,73,2000,1500,1500),(211,5,73,2000,1500,1500),(212,5,74,2000,1500,1500),(213,4,74,2000,1500,1500),(214,5,74,2000,1500,1500),(215,4,74,2000,1500,1500),(216,2,75,2000,1500,1500),(217,2,75,2000,1500,1500),(218,3,76,2000,1500,1500),(219,1,76,2000,1500,1500),(220,5,77,2000,1500,1500),(221,3,77,2000,1500,1500),(222,1,77,2000,1500,1500),(223,3,78,2000,1500,1500),(224,3,78,2000,1500,1500),(225,4,79,2000,1500,1500),(226,4,79,2000,1500,1500),(227,2,79,2000,1500,1500),(228,2,80,2000,1500,1500),(229,2,80,2000,1500,1500),(230,3,81,2000,1500,1500),(231,5,81,2000,1500,1500),(232,1,81,2000,1500,1500),(233,4,81,2000,1500,1500),(234,4,82,2000,1500,1500),(235,4,82,2000,1500,1500),(236,5,83,2000,1500,1500),(237,3,84,2000,1500,1500),(238,4,84,2000,1500,1500),(239,2,85,2000,1500,1500),(240,4,85,2000,1500,1500),(241,1,86,2000,1500,1500),(242,1,87,2000,1500,1500),(243,2,87,2000,1500,1500),(244,4,88,2000,1500,1500),(245,3,88,2000,1500,1500),(246,2,88,2000,1500,1500),(247,3,89,2000,1500,1500),(248,2,89,2000,1500,1500),(249,4,90,2000,1500,1500),(250,4,90,2000,1500,1500),(251,5,91,2000,1500,1500),(252,1,91,2000,1500,1500),(253,4,92,2000,1500,1500),(254,4,92,2000,1500,1500),(255,4,92,2000,1500,1500),(256,5,93,2000,1500,1500),(257,5,93,2000,1500,1500),(258,5,93,2000,1500,1500),(259,3,94,2000,1500,1500),(260,4,94,2000,1500,1500),(261,4,94,2000,1500,1500),(262,5,94,2000,1500,1500),(263,3,95,2000,1500,1500),(264,4,95,2000,1500,1500),(265,4,96,2000,1500,1500),(266,4,97,2000,1500,1500),(267,3,97,2000,1500,1500),(268,4,97,2000,1500,1500),(269,3,98,2000,1500,1500),(270,3,98,2000,1500,1500),(271,4,99,2000,1500,1500),(272,2,99,2000,1500,1500),(273,2,100,2000,1500,1500),(274,1,100,2000,1500,1500),(275,3,100,2000,1500,1500),(276,3,101,2000,1500,1500),(277,2,101,2000,1500,1500),(278,3,101,2000,1500,1500),(279,3,102,2000,1500,1500),(280,4,102,2000,1500,1500),(281,1,103,2000,1500,1500),(282,4,103,2000,1500,1500),(283,1,104,2000,1500,1500),(284,3,104,2000,1500,1500),(285,4,104,2000,1500,1500),(286,3,105,2000,1500,1500),(287,5,105,2000,1500,1500),(288,3,105,2000,1500,1500),(289,3,105,2000,1500,1500),(290,4,106,2000,1500,1500),(291,4,106,2000,1500,1500),(292,1,107,2000,1500,1500),(293,1,107,2000,1500,1500),(294,3,108,2000,1500,1500),(295,3,109,2000,1500,1500),(296,1,109,2000,1500,1500),(297,1,110,2000,1500,1500),(298,2,110,2000,1500,1500),(299,2,110,2000,1500,1500),(300,2,111,2000,1500,1500),(396,4,297,2001,1500,1500),(397,4,298,2001,1500,1500),(398,4,299,2001,1500,1500);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `routes` (
  `controller` varchar(20) DEFAULT NULL,
  `route` varchar(20) DEFAULT NULL,
  `private` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
INSERT INTO `routes` VALUES ('Houses','create',1),('Houses','get',1),('Houses','ajax',1),('Houses','update',1),('Houses','delete',1),('Index','login',0),('Houses','list',1);
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `authorised` tinyint(4) DEFAULT '0',
  `role` varchar(10) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `blockExpires` datetime DEFAULT NULL,
  `loginAttempts` int(11) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2y$10$QS.T0VwA5/a78y2s6m2Yiulu2bhGPzH/W1ay7QslX830PO4RTCVj.',1,'Admin',NULL,NULL,0,NULL),(2,'user','$2y$10$QS.T0VwA5/a78y2s6m2Yiulu2bhGPzH/W1ay7QslX830PO4RTCVj.',1,'User',NULL,NULL,0,NULL),(3,'guest','$2y$10$QS.T0VwA5/a78y2s6m2Yiulu2bhGPzH/W1ay7QslX830PO4RTCVj.',1,'Guest',NULL,NULL,NULL,NULL),(4,'user2','$2y$10$QS.T0VwA5/a78y2s6m2Yiulu2bhGPzH/W1ay7QslX830PO4RTCVj.',1,'User',NULL,NULL,NULL,NULL),(5,'user3','$2y$10$QS.T0VwA5/a78y2s6m2Yiulu2bhGPzH/W1ay7QslX830PO4RTCVj.',1,'User',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-30 20:15:30
